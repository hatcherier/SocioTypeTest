﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFA2
{
    public partial class Form1 : MaterialForm
    {
        int lvl, lvlS, lvlSP, first, second, group;
        string level;

        public Form1()
        {
            InitializeComponent();

            // Establishing MaterialSkin style (Material Design)
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);

            lvl = 0;
            first = 0;
            second = 0;

            group = 0;

            materialFlatButton1.Text = "Начать";

            materialFlatButton2.Text = "Способности";
            materialFlatButton3.Text = "Проблемы";
            materialFlatButton5.Text = "Не ожидать";
            materialFlatButton6.Text = "Деятельность";

            materialFlatButton4.Text = "Группа";

            materialLabel9.Text = "Этап 1 из 72";

            materialCheckBox1.Visible = false;
            materialCheckBox2.Visible = false;

            materialLabel3.Visible = false;
            materialLabel4.Visible = false;
            materialLabel5.Visible = false;
            materialLabel6.Visible = false;

            materialLabel7.Visible = false;
            materialLabel8.Visible = false;

            materialFlatButton2.Visible = false;
            materialFlatButton3.Visible = false;
            materialFlatButton5.Visible = false;
            materialFlatButton6.Visible = false;

            materialFlatButton4.Visible = false;

            materialLabel9.Visible = false;
        }

        // Uncheck checkbox if other one is already checked
        private void firstCheck(object sender, EventArgs e)
        {
            if (materialCheckBox2.Checked == true)
            {
                materialCheckBox2.Checked = false;
            }
        }

        private void secondCheck(object sender, EventArgs e)
        {
            if (materialCheckBox1.Checked == true)
            {
                materialCheckBox1.Checked = false;
            }
        }
        /* Monitoring state of checkboxes, checking if first
        checkbox was checked more times than the second
        */
        private void calc()
        {
            if (first + second == 9)
            {
                first = 0;
                second = 0;
            }
            if (materialCheckBox1.Checked)
            {
                first++;
            }
            else
            {
                if (materialCheckBox2.Checked)
                {
                    second++;
                }
            }
            // group #1
            if (first > second && lvl == 18)
            {
                materialLabel3.Text = Properties.Resources.rational;
            }
            if (second > first && lvl == 18)
            {
                materialLabel3.Text = Properties.Resources.irational;
            }
            // group #2
            if (first > second && lvl == 37)
            {
                materialLabel4.Text = Properties.Resources.extro;
            }
            if (second > first && lvl == 37)
            {
                materialLabel4.Text = Properties.Resources.intro;
            }
            // group #3
            if (first > second && lvl == 55)
            {
                materialLabel5.Text = Properties.Resources.sens;
            }
            if (second > first && lvl == 55)
            {
                materialLabel5.Text = Properties.Resources.intuit;
            }
            // group #4
            if (first > second && lvl > 71)
            {
                materialLabel6.Text = Properties.Resources.logica;
            }
            if (second > first && lvl > 71)
            {
                materialLabel6.Text = Properties.Resources.etica;
            }
        }

        private void startOrNext(object sender, EventArgs e)
        {
            if (lvl > 0 && lvl < 72)
            {
                // stop executing if checkboxes is unchecked
                if (materialCheckBox1.Checked == false && materialCheckBox2.Checked == false)
                {
                    return;
                }

                // Displaying progress of statements
                lvlS = lvl;
                lvlSP = lvlS + 1;
                level = lvlSP.ToString();
                materialLabel9.Text = "Этап " + level + " из 72";
            }

            // Checking if the test is over
            if (lvl == 72)
            {
                calc();

                endTest();
                materialFlatButton1.Text = "Результаты";

                materialCheckBox1.Visible = false;
                materialCheckBox2.Visible = false;

                materialLabel9.Visible = false;
            }

            if (lvl < 1)
            {
                materialCheckBox1.Visible = true;
                materialCheckBox2.Visible = true;

                materialLabel9.Visible = true;

                materialFlatButton1.Text = "Далее";
                lvl++;

                materialCheckBox1.Text = Properties.Resources.q1;
                materialCheckBox2.Text = Properties.Resources.q2;
            } else
            {
                calc();

                materialCheckBox1.Checked = false;
                materialCheckBox2.Checked = false;

                group2();
            }
        }

        private void group2()
        {
            if (lvl == 1)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q3;
                materialCheckBox2.Text = Properties.Resources.q4;
            } else
            {
                group3();
            }
        }

        private void group3()
        {
            if (lvl == 2)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q5;
                materialCheckBox2.Text = Properties.Resources.q6;
            } else
            {
                group4();
            }
        }

        private void group4()
        {
            if (lvl == 3)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q7;
                materialCheckBox2.Text = Properties.Resources.q8;
            }
            else
            {
                group5();
            }
        }

        private void group5()
        {
            if (lvl == 4)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q9;
                materialCheckBox2.Text = Properties.Resources.q10;
            }
            else
            {
                group6();
            }
        }

        private void group6()
        {
            if (lvl == 5)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q11;
                materialCheckBox2.Text = Properties.Resources.q12;
            }
            else
            {
                group7();
            }
        }

        private void group7()
        {
            if (lvl == 6)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q13;
                materialCheckBox2.Text = Properties.Resources.q14;
            }
            else
            {
                group8();
            }
        }

        private void group8()
        {
            if (lvl == 7)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q15;
                materialCheckBox2.Text = Properties.Resources.q16;
            }
            else
            {
                group9();
            }
        }

        private void group9()
        {
            if (lvl == 8)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q17;
                materialCheckBox2.Text = Properties.Resources.q18;
            }
            else
            {
                group10();
            }
        }

        private void group10()
        {
            if (lvl == 9)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q19;
                materialCheckBox2.Text = Properties.Resources.q20;
            }
            else
            {
                group11();
            }
        }

        private void group11()
        {
            if (lvl == 10)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q21;
                materialCheckBox2.Text = Properties.Resources.q22;
            }
            else
            {
                group12();
            }
        }

        private void group12()
        {
            if (lvl == 11)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q23;
                materialCheckBox2.Text = Properties.Resources.q24;
            }
            else
            {
                group13();
            }
        }

        private void group13()
        {
            if (lvl == 12)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q25;
                materialCheckBox2.Text = Properties.Resources.q26;
            }
            else
            {
                group14();
            }
        }

        private void group14()
        {
            if (lvl == 13)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q27;
                materialCheckBox2.Text = Properties.Resources.q28;
            }
            else
            {
                group15();
            }
        }

        private void group15()
        {
            if (lvl == 14)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q29;
                materialCheckBox2.Text = Properties.Resources.q30;
            }
            else
            {
                group16();
            }
        }

        private void group16()
        {
            if (lvl == 15)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q31;
                materialCheckBox2.Text = Properties.Resources.q32;
            }
            else
            {
                group17();
            }
        }

        private void group17()
        {
            if (lvl == 16)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q33;
                materialCheckBox2.Text = Properties.Resources.q34;
            }
            else
            {
                group18();
            }
        }

        private void group18()
        {
            if (lvl == 17)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q35;
                materialCheckBox2.Text = Properties.Resources.q36;
            }
            else
            {
                group19();
            }
        }

        private void group19()
        {
            if (lvl == 18)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q37;
                materialCheckBox2.Text = Properties.Resources.q38;
            }
            else
            {
                group20();
            }
        }

        private void group20()
        {
            if (lvl == 19)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q39;
                materialCheckBox2.Text = Properties.Resources.q40;
            }
            else
            {
                group21();
            }
        }

        private void group21()
        {
            if (lvl == 20)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q41;
                materialCheckBox2.Text = Properties.Resources.q42;
            }
            else
            {
                group22();
            }
        }

        private void group22()
        {
            if (lvl == 21)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q43;
                materialCheckBox2.Text = Properties.Resources.q44;
            }
            else
            {
                group23();
            }
        }

        private void group23()
        {
            if (lvl == 22)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q45;
                materialCheckBox2.Text = Properties.Resources.q46;
            }
            else
            {
                group24();
            }
        }

        private void group24()
        {
            if (lvl == 23)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q47;
                materialCheckBox2.Text = Properties.Resources.q48;
            }
            else
            {
                group25();
            }
        }

        private void group25()
        {
            if (lvl == 24)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q49;
                materialCheckBox2.Text = Properties.Resources.q50;
            }
            else
            {
                group26();
            }
        }

        private void group26()
        {
            if (lvl == 25)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q51;
                materialCheckBox2.Text = Properties.Resources.q52;
            }
            else
            {
                group27();
            }
        }

        private void group27()
        {
            if (lvl == 26)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q53;
                materialCheckBox2.Text = Properties.Resources.q54;
            }
            else
            {
                group28();
            }
        }

        private void group28()
        {
            if (lvl == 27)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q55;
                materialCheckBox2.Text = Properties.Resources.q56;
            }
            else
            {
                group29();
            }
        }

        private void group29()
        {
            if (lvl == 28)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q57;
                materialCheckBox2.Text = Properties.Resources.q58;
            }
            else
            {
                group30();
            }
        }

        private void group30()
        {
            if (lvl == 29)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q59;
                materialCheckBox2.Text = Properties.Resources.q60;
            }
            else
            {
                group31();
            }
        }

        private void group31()
        {
            if (lvl == 30)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q61;
                materialCheckBox2.Text = Properties.Resources.q62;
            }
            else
            {
                group32();
            }
        }

        private void group32()
        {
            if (lvl == 31)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q63;
                materialCheckBox2.Text = Properties.Resources.q64;
            }
            else
            {
                group33();
            }
        }

        private void group33()
        {
            if (lvl == 32)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q65;
                materialCheckBox2.Text = Properties.Resources.q66;
            }
            else
            {
                group34();
            }
        }

        private void group34()
        {
            if (lvl == 33)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q67;
                materialCheckBox2.Text = Properties.Resources.q68;
            }
            else
            {
                group35();
            }
        }

        private void group35()
        {
            if (lvl == 34)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q69;
                materialCheckBox2.Text = Properties.Resources.q70;
            }
            else
            {
                group36();
            }
        }

        private void group36()
        {
            if (lvl == 35)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q71;
                materialCheckBox2.Text = Properties.Resources.q72;
            }
            else
            {
                group37();
            }
        }

        private void group37()
        {
            if (lvl == 36)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q73;
                materialCheckBox2.Text = Properties.Resources.q74;
            }
            else
            {
                group38();
            }
        }

        private void group38()
        {
            if (lvl == 37)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q75;
                materialCheckBox2.Text = Properties.Resources.q76;
            }
            else
            {
                group39();
            }
        }

        private void group39()
        {
            if (lvl == 38)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q77;
                materialCheckBox2.Text = Properties.Resources.q78;
            }
            else
            {
                group40();
            }
        }

        private void group40()
        {
            if (lvl == 39)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q79;
                materialCheckBox2.Text = Properties.Resources.q80;
            }
            else
            {
                group41();
            }
        }

        private void group41()
        {
            if (lvl == 40)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q81;
                materialCheckBox2.Text = Properties.Resources.q82;
            }
            else
            {
                group42();
            }
        }

        private void group42()
        {
            if (lvl == 41)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q83;
                materialCheckBox2.Text = Properties.Resources.q84;
            }
            else
            {
                group43();
            }
        }

        private void group43()
        {
            if (lvl == 42)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q85;
                materialCheckBox2.Text = Properties.Resources.q86;
            }
            else
            {
                group44();
            }
        }

        private void group44()
        {
            if (lvl == 43)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q87;
                materialCheckBox2.Text = Properties.Resources.q88;
            }
            else
            {
                group45();
            }
        }

        private void group45()
        {
            if (lvl == 44)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q89;
                materialCheckBox2.Text = Properties.Resources.q90;
            }
            else
            {
                group46();
            }
        }

        private void group46()
        {
            if (lvl == 45)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q91;
                materialCheckBox2.Text = Properties.Resources.q92;
            }
            else
            {
                group47();
            }
        }

        private void group47()
        {
            if (lvl == 46)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q93;
                materialCheckBox2.Text = Properties.Resources.q94;
            }
            else
            {
                group48();
            }
        }

        private void group48()
        {
            if (lvl == 47)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q95;
                materialCheckBox2.Text = Properties.Resources.q96;
            }
            else
            {
                group49();
            }
        }

        private void group49()
        {
            if (lvl == 48)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q97;
                materialCheckBox2.Text = Properties.Resources.q98;
            }
            else
            {
                group50();
            }
        }

        private void group50()
        {
            if (lvl == 49)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q99;
                materialCheckBox2.Text = Properties.Resources.q100;
            }
            else
            {
                group51();
            }
        }

        private void group51()
        {
            if (lvl == 50)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q101;
                materialCheckBox2.Text = Properties.Resources.q102;
            }
            else
            {
                group52();
            }
        }

        private void group52()
        {
            if (lvl == 51)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q103;
                materialCheckBox2.Text = Properties.Resources.q104;
            }
            else
            {
                group53();
            }
        }

        private void group53()
        {
            if (lvl == 52)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q105;
                materialCheckBox2.Text = Properties.Resources.q106;
            }
            else
            {
                group54();
            }
        }

        private void group54()
        {
            if (lvl == 53)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q107;
                materialCheckBox2.Text = Properties.Resources.q108;
            }
            else
            {
                group55();
            }
        }

        private void group55()
        {
            if (lvl == 54)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q109;
                materialCheckBox2.Text = Properties.Resources.q110;
            }
            else
            {
                group56();
            }
        }

        private void group56()
        {
            if (lvl == 55)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q111;
                materialCheckBox2.Text = Properties.Resources.q112;
            }
            else
            {
                group57();
            }
        }

        private void group57()
        {
            if (lvl == 56)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q113;
                materialCheckBox2.Text = Properties.Resources.q114;
            }
            else
            {
                group58();
            }
        }

        private void group58()
        {
            if (lvl == 57)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q115;
                materialCheckBox2.Text = Properties.Resources.q116;
            }
            else
            {
                group59();
            }
        }

        private void group59()
        {
            if (lvl == 58)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q117;
                materialCheckBox2.Text = Properties.Resources.q118;
            }
            else
            {
                group60();
            }
        }

        private void group60()
        {
            if (lvl == 59)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q119;
                materialCheckBox2.Text = Properties.Resources.q120;
            }
            else
            {
                group61();
            }
        }

        private void group61()
        {
            if (lvl == 60)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q121;
                materialCheckBox2.Text = Properties.Resources.q122;
            }
            else
            {
                group62();
            }
        }

        private void group62()
        {
            if (lvl == 61)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q123;
                materialCheckBox2.Text = Properties.Resources.q124;
            }
            else
            {
                group63();
            }
        }

        private void group63()
        {
            if (lvl == 62)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q125;
                materialCheckBox2.Text = Properties.Resources.q126;
            }
            else
            {
                group64();
            }
        }

        private void group64()
        {
            if (lvl == 63)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q127;
                materialCheckBox2.Text = Properties.Resources.q128;
            }
            else
            {
                group65();
            }
        }

        private void group65()
        {
            if (lvl == 64)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q129;
                materialCheckBox2.Text = Properties.Resources.q130;
            }
            else
            {
                group66();
            }
        }

        private void group66()
        {
            if (lvl == 65)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q131;
                materialCheckBox2.Text = Properties.Resources.q132;
            }
            else
            {
                group67();
            }
        }

        private void group67()
        {
            if (lvl == 66)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q133;
                materialCheckBox2.Text = Properties.Resources.q134;
            }
            else
            {
                group68();
            }
        }

        private void group68()
        {
            if (lvl == 67)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q135;
                materialCheckBox2.Text = Properties.Resources.q136;
            }
            else
            {
                group69();
            }
        }

        private void group69()
        {
            if (lvl == 68)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q137;
                materialCheckBox2.Text = Properties.Resources.q138;
            }
            else
            {
                group70();
            }
        }

        private void group70()
        {
            if (lvl == 69)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q139;
                materialCheckBox2.Text = Properties.Resources.q140;
            }
            else
            {
                group71();
            }
        }

        private void group71()
        {
            if (lvl == 70)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q141;
                materialCheckBox2.Text = Properties.Resources.q142;
            }
            else
            {
                group72();
            }
        }

        // Display the last statements
        private void group72()
        {
            if (lvl == 71)
            {
                lvl++;
                materialCheckBox1.Text = Properties.Resources.q143;
                materialCheckBox2.Text = Properties.Resources.q144;
            }
        }

        private void endTest()
        {
            if (materialLabel7.Text == "materialLabel7")
            {

            } else
            {
                materialLabel7.Visible = true;
                materialLabel8.Visible = true;

                materialFlatButton1.Visible = false;
            }

            show();
        }

        // Establishing the group (Квадра) of the tester
        private void setQu(object sender, EventArgs e)
        {
            group++;
            materialFlatButton4.Visible = false;

            if (materialLabel7.Text == Properties.Resources.c01)
            {
                materialLabel7.Text = "Квадра #I (Альфа)";
                materialLabel8.Text = Properties.Resources.qu1;
            }

            if (materialLabel7.Text == Properties.Resources.c02)
            {
                materialLabel7.Text = "Квадра I (Альфа)";
                materialLabel8.Text = Properties.Resources.qu1;
            }

            if (materialLabel7.Text == Properties.Resources.c03)
            {
                materialLabel7.Text = "Квадра I (Альфа)";
                materialLabel8.Text = Properties.Resources.qu1;
            }

            if (materialLabel7.Text == Properties.Resources.c04)
            {
                materialLabel7.Text = "Квадра I (Альфа)";
                materialLabel8.Text = Properties.Resources.qu1;
            }

            if (materialLabel7.Text == Properties.Resources.c05)
            {
                materialLabel7.Text = "Квадра II (Бета)";
                materialLabel8.Text = Properties.Resources.qu2;
            }

            if (materialLabel7.Text == Properties.Resources.c06)
            {
                materialLabel7.Text = "Квадра II (Бета)";
                materialLabel8.Text = Properties.Resources.qu2;
            }

            if (materialLabel7.Text == Properties.Resources.c07)
            {
                materialLabel7.Text = "Квадра II (Бета)";
                materialLabel8.Text = Properties.Resources.qu2;
            }

            if (materialLabel7.Text == Properties.Resources.c08)
            {
                materialLabel7.Text = "Квадра II (Бета)";
                materialLabel8.Text = Properties.Resources.qu2;
            }

            if (materialLabel7.Text == Properties.Resources.c09)
            {
                materialLabel7.Text = "Квадра III (Гамма)";
                materialLabel8.Text = Properties.Resources.qu3;
            }

            if (materialLabel7.Text == Properties.Resources.c10)
            {
                materialLabel7.Text = "Квадра III (Гамма)";
                materialLabel8.Text = Properties.Resources.qu3;
            }

            if (materialLabel7.Text == Properties.Resources.c11)
            {
                materialLabel7.Text = "Квадра III (Гамма)";
                materialLabel8.Text = Properties.Resources.qu3;
            }

            if (materialLabel7.Text == Properties.Resources.c12)
            {
                materialLabel7.Text = "Квадра III (Гамма)";
                materialLabel8.Text = Properties.Resources.qu3;
            }

            if (materialLabel7.Text == Properties.Resources.c13)
            {
                materialLabel7.Text = "Квадра IV (Дельта)";
                materialLabel8.Text = Properties.Resources.qu4;
            }

            if (materialLabel7.Text == Properties.Resources.c14)
            {
                materialLabel7.Text = "Квадра IV (Дельта)";
                materialLabel8.Text = Properties.Resources.qu4;
            }

            if (materialLabel7.Text == Properties.Resources.c15)
            {
                materialLabel7.Text = "Квадра IV (Дельта)";
                materialLabel8.Text = Properties.Resources.qu4;
            }

            if (materialLabel7.Text == Properties.Resources.c16)
            {
                materialLabel7.Text = "Квадра IV (Дельта)";
                materialLabel8.Text = Properties.Resources.qu4;
            }

            materialFlatButton2.Text = "Назад";

            materialFlatButton2.Enabled = true;
            materialFlatButton3.Visible = false;
            materialFlatButton5.Visible = false;
            materialFlatButton6.Visible = false;
        }

        // Display discription of tester's sociotype
        private void show()
        {
            if (materialLabel5.Text == Properties.Resources.intuit &&
                materialLabel6.Text == Properties.Resources.logica &&
                materialLabel4.Text == Properties.Resources.extro)
            {
                materialLabel7.Text = Properties.Resources.c01;
            }

            if (materialLabel5.Text == Properties.Resources.sens &&
                materialLabel6.Text == Properties.Resources.etica &&
                materialLabel4.Text == Properties.Resources.intro)
            {
                materialLabel7.Text = Properties.Resources.c02;
            }

            if (materialLabel6.Text == Properties.Resources.etica &&
                materialLabel5.Text == Properties.Resources.sens &&
                materialLabel4.Text == Properties.Resources.extro)
            {
                materialLabel7.Text = Properties.Resources.c03;
            }

            if (materialLabel6.Text == Properties.Resources.logica &&
                materialLabel5.Text == Properties.Resources.intuit &&
                materialLabel4.Text == Properties.Resources.intro)
            {
                materialLabel7.Text = Properties.Resources.c04;
            }

            if (materialLabel6.Text == Properties.Resources.etica &&
                materialLabel5.Text == Properties.Resources.intuit &&
                materialLabel4.Text == Properties.Resources.extro)
            {
                materialLabel7.Text = Properties.Resources.c05;
            }

            if (materialLabel6.Text == Properties.Resources.logica &&
                materialLabel5.Text == Properties.Resources.sens &&
                materialLabel4.Text == Properties.Resources.intro)
            {
                materialLabel7.Text = Properties.Resources.c06;
            }

            if (materialLabel5.Text == Properties.Resources.sens &&
                materialLabel6.Text == Properties.Resources.logica &&
                materialLabel4.Text == Properties.Resources.extro)
            {
                materialLabel7.Text = Properties.Resources.c07;
            }

            if (materialLabel5.Text == Properties.Resources.intuit &&
                materialLabel6.Text == Properties.Resources.etica &&
                materialLabel4.Text == Properties.Resources.intro)
            {
                materialLabel7.Text = Properties.Resources.c08;
            }

            if (materialLabel5.Text == Properties.Resources.sens &&
                materialLabel6.Text == Properties.Resources.etica &&
                materialLabel4.Text == Properties.Resources.extro)
            {
                materialLabel7.Text = Properties.Resources.c09;
            }

            if (materialLabel5.Text == Properties.Resources.intuit &&
                materialLabel6.Text == Properties.Resources.logica &&
                materialLabel4.Text == Properties.Resources.intro)
            {
                materialLabel7.Text = Properties.Resources.c10;
            }

            if (materialLabel6.Text == Properties.Resources.logica &&
                materialLabel5.Text == Properties.Resources.intuit &&
                materialLabel4.Text == Properties.Resources.extro)
            {
                materialLabel7.Text = Properties.Resources.c11;
            }

            if (materialLabel6.Text == Properties.Resources.etica &&
                materialLabel5.Text == Properties.Resources.sens &&
                materialLabel4.Text == Properties.Resources.intro)
            {
                materialLabel7.Text = Properties.Resources.c12;
            }

            if (materialLabel6.Text == Properties.Resources.logica &&
                materialLabel5.Text == Properties.Resources.sens &&
                materialLabel4.Text == Properties.Resources.extro)
            {
                materialLabel7.Text = Properties.Resources.c13;
            }

            if (materialLabel6.Text == Properties.Resources.etica &&
                materialLabel5.Text == Properties.Resources.intuit &&
                materialLabel4.Text == Properties.Resources.intro)
            {
                materialLabel7.Text = Properties.Resources.c14;
            }

            if (materialLabel5.Text == Properties.Resources.intuit &&
                materialLabel6.Text == Properties.Resources.etica &&
                materialLabel4.Text == Properties.Resources.extro)
            {
                materialLabel7.Text = Properties.Resources.c15;
            }

            if (materialLabel5.Text == Properties.Resources.sens &&
                materialLabel6.Text == Properties.Resources.logica &&
                materialLabel4.Text == Properties.Resources.intro)
            {
                materialLabel7.Text = Properties.Resources.c16;
            }

            surf();
        }

        // Navigate to Способности
        private void surf()
        {
            if (materialLabel7.Text == Properties.Resources.c01)
            {
                materialLabel8.Text = Properties.Resources.c01a;
            }

            if (materialLabel7.Text == Properties.Resources.c02)
            {
                materialLabel8.Text = Properties.Resources.c02a;
            }

            if (materialLabel7.Text == Properties.Resources.c03)
            {
                materialLabel8.Text = Properties.Resources.c03a;
            }

            if (materialLabel7.Text == Properties.Resources.c04)
            {
                materialLabel8.Text = Properties.Resources.c04a;
            }

            if (materialLabel7.Text == Properties.Resources.c05)
            {
                materialLabel8.Text = Properties.Resources.c05a;
            }

            if (materialLabel7.Text == Properties.Resources.c06)
            {
                materialLabel8.Text = Properties.Resources.c06a;
            }

            if (materialLabel7.Text == Properties.Resources.c07)
            {
                materialLabel8.Text = Properties.Resources.c07a;
            }

            if (materialLabel7.Text == Properties.Resources.c08)
            {
                materialLabel8.Text = Properties.Resources.c08a;
            }

            if (materialLabel7.Text == Properties.Resources.c09)
            {
                materialLabel8.Text = Properties.Resources.c09a;
            }

            if (materialLabel7.Text == Properties.Resources.c10)
            {
                materialLabel8.Text = Properties.Resources.c10a;
            }

            if (materialLabel7.Text == Properties.Resources.c11)
            {
                materialLabel8.Text = Properties.Resources.c11a;
            }

            if (materialLabel7.Text == Properties.Resources.c12)
            {
                materialLabel8.Text = Properties.Resources.c12a;
            }

            if (materialLabel7.Text == Properties.Resources.c13)
            {
                materialLabel8.Text = Properties.Resources.c13a;
            }

            if (materialLabel7.Text == Properties.Resources.c14)
            {
                materialLabel8.Text = Properties.Resources.c14a;
            }

            if (materialLabel7.Text == Properties.Resources.c15)
            {
                materialLabel8.Text = Properties.Resources.c15a;
            }

            if (materialLabel7.Text == Properties.Resources.c16)
            {
                materialLabel8.Text = Properties.Resources.c16a;
            }

            if (materialFlatButton1.Text == "Результаты")
            {
                materialFlatButton2.Visible = true;
                materialFlatButton2.Enabled = false;
                materialFlatButton3.Visible = true;
                materialFlatButton5.Visible = true;
                materialFlatButton6.Visible = true;

                materialFlatButton4.Visible = true;
            }
        }

        private void materialFlatButton2_Click(object sender, EventArgs e)
        {
            materialFlatButton2.Enabled = false;
            materialFlatButton3.Enabled = true;
            materialFlatButton5.Enabled = true;
            materialFlatButton6.Enabled = true;

            surf();
            materialFlatButton2.Text = "Способности";
            if (group == 1)
            {
                group--;
                show();
            }
        }

        // Navigate to Проблемы
        private void materialFlatButton3_Click(object sender, EventArgs e)
        {
            materialFlatButton2.Enabled = true;
            materialFlatButton3.Enabled = false;
            materialFlatButton5.Enabled = true;
            materialFlatButton6.Enabled = true;

            if (materialLabel7.Text == Properties.Resources.c01)
            {
                materialLabel8.Text = Properties.Resources.c01b;
            }

            if (materialLabel7.Text == Properties.Resources.c02)
            {
                materialLabel8.Text = Properties.Resources.c02b;
            }

            if (materialLabel7.Text == Properties.Resources.c03)
            {
                materialLabel8.Text = Properties.Resources.c03b;
            }

            if (materialLabel7.Text == Properties.Resources.c04)
            {
                materialLabel8.Text = Properties.Resources.c04b;
            }

            if (materialLabel7.Text == Properties.Resources.c05)
            {
                materialLabel8.Text = Properties.Resources.c05b;
            }

            if (materialLabel7.Text == Properties.Resources.c06)
            {
                materialLabel8.Text = Properties.Resources.c06b;
            }

            if (materialLabel7.Text == Properties.Resources.c07)
            {
                materialLabel8.Text = Properties.Resources.c07b;
            }

            if (materialLabel7.Text == Properties.Resources.c08)
            {
                materialLabel8.Text = Properties.Resources.c08b;
            }

            if (materialLabel7.Text == Properties.Resources.c09)
            {
                materialLabel8.Text = Properties.Resources.c09b;
            }

            if (materialLabel7.Text == Properties.Resources.c10)
            {
                materialLabel8.Text = Properties.Resources.c10b;
            }

            if (materialLabel7.Text == Properties.Resources.c11)
            {
                materialLabel8.Text = Properties.Resources.c11b;
            }

            if (materialLabel7.Text == Properties.Resources.c12)
            {
                materialLabel8.Text = Properties.Resources.c12b;
            }

            if (materialLabel7.Text == Properties.Resources.c13)
            {
                materialLabel8.Text = Properties.Resources.c13b;
            }

            if (materialLabel7.Text == Properties.Resources.c14)
            {
                materialLabel8.Text = Properties.Resources.c14b;
            }

            if (materialLabel7.Text == Properties.Resources.c15)
            {
                materialLabel8.Text = Properties.Resources.c15b;
            }

            if (materialLabel7.Text == Properties.Resources.c16)
            {
                materialLabel8.Text = Properties.Resources.c16b;
            }
        }

        // Navigate to Не ожидать
        private void materialFlatButton5_Click(object sender, EventArgs e)
        {
            materialFlatButton2.Enabled = true;
            materialFlatButton3.Enabled = true;
            materialFlatButton5.Enabled = false;
            materialFlatButton6.Enabled = true;

            if (materialLabel7.Text == Properties.Resources.c01)
            {
                materialLabel8.Text = Properties.Resources.c01c;
            }

            if (materialLabel7.Text == Properties.Resources.c02)
            {
                materialLabel8.Text = Properties.Resources.c02c;
            }

            if (materialLabel7.Text == Properties.Resources.c03)
            {
                materialLabel8.Text = Properties.Resources.c03c;
            }

            if (materialLabel7.Text == Properties.Resources.c04)
            {
                materialLabel8.Text = Properties.Resources.c04c;
            }

            if (materialLabel7.Text == Properties.Resources.c05)
            {
                materialLabel8.Text = Properties.Resources.c05c;
            }

            if (materialLabel7.Text == Properties.Resources.c06)
            {
                materialLabel8.Text = Properties.Resources.c06c;
            }

            if (materialLabel7.Text == Properties.Resources.c07)
            {
                materialLabel8.Text = Properties.Resources.c07c;
            }

            if (materialLabel7.Text == Properties.Resources.c08)
            {
                materialLabel8.Text = Properties.Resources.c08c;
            }

            if (materialLabel7.Text == Properties.Resources.c09)
            {
                materialLabel8.Text = Properties.Resources.c09c;
            }

            if (materialLabel7.Text == Properties.Resources.c10)
            {
                materialLabel8.Text = Properties.Resources.c10c;
            }

            if (materialLabel7.Text == Properties.Resources.c11)
            {
                materialLabel8.Text = Properties.Resources.c11c;
            }

            if (materialLabel7.Text == Properties.Resources.c12)
            {
                materialLabel8.Text = Properties.Resources.c12c;
            }

            if (materialLabel7.Text == Properties.Resources.c13)
            {
                materialLabel8.Text = Properties.Resources.c13c;
            }

            if (materialLabel7.Text == Properties.Resources.c14)
            {
                materialLabel8.Text = Properties.Resources.c14c;
            }

            if (materialLabel7.Text == Properties.Resources.c15)
            {
                materialLabel8.Text = Properties.Resources.c15c;
            }

            if (materialLabel7.Text == Properties.Resources.c16)
            {
                materialLabel8.Text = Properties.Resources.c16c;
            }
        }

        // Navigate to Деятельность
        private void materialFlatButton6_Click(object sender, EventArgs e)
        {
            materialFlatButton2.Enabled = true;
            materialFlatButton3.Enabled = true;
            materialFlatButton5.Enabled = true;
            materialFlatButton6.Enabled = false;

            if (materialLabel7.Text == Properties.Resources.c01)
            {
                materialLabel8.Text = Properties.Resources.c01d;
            }

            if (materialLabel7.Text == Properties.Resources.c02)
            {
                materialLabel8.Text = Properties.Resources.c02d;
            }

            if (materialLabel7.Text == Properties.Resources.c03)
            {
                materialLabel8.Text = Properties.Resources.c03d;
            }

            if (materialLabel7.Text == Properties.Resources.c04)
            {
                materialLabel8.Text = Properties.Resources.c04d;
            }

            if (materialLabel7.Text == Properties.Resources.c05)
            {
                materialLabel8.Text = Properties.Resources.c05d;
            }

            if (materialLabel7.Text == Properties.Resources.c06)
            {
                materialLabel8.Text = Properties.Resources.c06d;
            }

            if (materialLabel7.Text == Properties.Resources.c07)
            {
                materialLabel8.Text = Properties.Resources.c07d;
            }

            if (materialLabel7.Text == Properties.Resources.c08)
            {
                materialLabel8.Text = Properties.Resources.c08d;
            }

            if (materialLabel7.Text == Properties.Resources.c09)
            {
                materialLabel8.Text = Properties.Resources.c09d;
            }

            if (materialLabel7.Text == Properties.Resources.c10)
            {
                materialLabel8.Text = Properties.Resources.c10d;
            }

            if (materialLabel7.Text == Properties.Resources.c11)
            {
                materialLabel8.Text = Properties.Resources.c11d;
            }

            if (materialLabel7.Text == Properties.Resources.c12)
            {
                materialLabel8.Text = Properties.Resources.c12d;
            }

            if (materialLabel7.Text == Properties.Resources.c13)
            {
                materialLabel8.Text = Properties.Resources.c13d;
            }

            if (materialLabel7.Text == Properties.Resources.c14)
            {
                materialLabel8.Text = Properties.Resources.c14d;
            }

            if (materialLabel7.Text == Properties.Resources.c15)
            {
                materialLabel8.Text = Properties.Resources.c15d;
            }

            if (materialLabel7.Text == Properties.Resources.c16)
            {
                materialLabel8.Text = Properties.Resources.c16d;
            }
        }
    }
}
