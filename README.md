# SocioTypeTest
Sociotype test using [site](http://socionika.info/) data (russian language)
## Windows
Download the [final](https://gitlab.com/hatcherier/SocioTypeTest/uploads/a0b35ac836cd0932e2642b39b2d1eb0d/sttest-2.2.0.zip) version
# Licenses
[GPL-3.0](https://gitlab.com/hatcherier/SocioTypeTest/blob/master/LICENSE)
## Third-party License
[MaterialSkin](https://github.com/IgnaceMaes/MaterialSkin) by [Ignace Maes](https://github.com/IgnaceMaes) under [MIT](https://github.com/IgnaceMaes/MaterialSkin/blob/master/LICENSE)
